<?php
// $Id: poker.admin.inc,v 1.0 2011/05/27 19:09:49 luke Exp $

function poker_admin_settings(){
    $form['descriptiontext'] = array(
	    '#type' => 'markup',
		'#value' => '<div style="font-size: 120%;font-weight:bold">If you would like PRIVATE tables and the ability to configure and customize your games, please visit
 <a href="http://www.justforfunsoftware.com/link.php?id=18">www.justforfunsoftware.com</a> to sign up for a low-cost rental plan, then insert your own customer id# above.
  PayPal, Mastercard and VISA payments accepted.</div>',
	);
	
    $form['poker_customer_number'] = array(
	    '#type' => 'textfield',
		'#title' => 'Customer Number',
		'#required' => false,
		'#default_value' => variable_get('poker_customer_number', 5),
	);
	
    $form['poker_iframe_or_link'] = array(
	    '#type' => 'radios',
		'#title' => 'Show as iframe or link',
		'#required' => true,
		'#options' => array('iframe' => 'iframe',
							'link' => 'link'), 
		'#default_value' => variable_get('poker_iframe_or_link', 'iframe'),
	);
	
    $form['poker_iframe_width'] = array(
	    '#type' => 'textfield',
		'#title' => 'Iframe Width',
		'#description' => 'Use "0" for auto width',
		'#required' => true,
		'#default_value' => variable_get('poker_iframe_width', 715),
	);
	
    $form['poker_iframe_height'] = array(
	    '#type' => 'textfield',
		'#title' => 'Iframe Height',
		'#description' => 'Use "0" for auto height',
		'#required' => true,
		'#default_value' => variable_get('poker_iframe_height', 510),
	);
	
    $form['poker_block_iframe_width'] = array(
	    '#type' => 'textfield',
		'#title' => 'Iframe Width as Block',
		'#description' => 'Use "0" for auto width',
		'#required' => true,
		'#default_value' => variable_get('poker_block_iframe_width', 715),
	);
	
    $form['poker_block_iframe_height'] = array(
	    '#type' => 'textfield',
		'#title' => 'Iframe Height as Block',
		'#description' => 'Use "0" for auto height',
		'#required' => true,
		'#default_value' => variable_get('poker_block_iframe_height', 510),
	);
	
    return system_settings_form($form);
}