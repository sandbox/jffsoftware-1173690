$Id: README.txt,v 1 2011/05/27 22:50:55 alexb Exp $

Justforfunsoftware.com Game Integration
=============

To integrate the game on Justforfunsoftware.com as inline iframe or popup window.


Usage
=====

1. Upload the file to drupal's module folder
2. Enable the module at "admin/build/modules"
3. Setup the Customer Number and other entries at "admin/settings/poker"
4. Try the game at the URL of "play"